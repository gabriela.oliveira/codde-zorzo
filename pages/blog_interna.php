<?php require_once('../components/header.php'); ?>

<main class="main-blog-interna" role="main">

	<section class="main__title blog-interna">
		<div class="container">
			<h2 class="title">Blog</h2>
			<p class="title__sub">Acompanhe dicas para manter o seu sorriso sempre belo e saudável</p>
		</div>
	</section>

	<section class="blog-interna__content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-sm-12">
					<div class="blog-interna__listing">
						<article class="blog__post--large clearfix">
							<div class="col-md-12 col-sm-12">
								<a class="blog__thumbnail" href="blog_interna.php">
									<img class="img-responsive" src="../assets/images/blog/blog-interna-post.png" title="" alt="">
								</a>
							</div>
							<div class="col-md-12 col-sm-12">
								<a class="blog__thumbnail" href="blog_interna.php">
									<h3 class="blog__title blog__title--large">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
									</h3>
									<p class="blog__author">
										Por: Gilberto Zorzo
									</p>

									<p class="blog__text blog__text--large">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
									</p>
									<p class="blog__text blog__text--large">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
									</p>
								</a>
								
								<div class="blog-interna__share">
									<p>Compartilhe:</p>

									<a href="" target="_blank"><i class="fa fa-linkedin"></i></a>
									<a href="" target="_blank"><i class="fa fa-facebook"></i></a>
									<a href="" target="_blank"><i class="fa fa-twitter"></i></a>
									<a href="" target="_blank"><i class="fa fa-instagram"></i></a>
								</div>
							</div>
						</article>
					</div>

					<div class="blog-interna__related">
						<h2 class="title title--small">Posts Relacionados</h2>

						<div class="col-md-6 col-sm-12">
							<article class="blog__post--large clearfix">
								<a class="blog__thumbnail" href="blog_interna.php">
									<h3 class="blog__title blog__title">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
									</h3>
									<p class="blog__author">
										Por: Gilberto Zorzo
									</p>
									<p class="blog__text blog__text">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut...
									</p>
								</a>
								<a class="blog__link blog__link" href="blog_interna.php">Continuar Lendo >></a>
							</article>
						</div>

						<div class="col-md-6 col-sm-12">
							<article class="blog__post--large clearfix">
								<a class="blog__thumbnail" href="blog_interna.php">
									<h3 class="blog__title blog__title">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
									</h3>
									<p class="blog__author">
										Por: Gilberto Zorzo
									</p>
									<p class="blog__text blog__text">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut...
									</p>
								</a>
								<a class="blog__link blog__link" href="blog_interna.php">Continuar Lendo >></a>
							</article>
						</div>

					</div>
				</div>

				<div class="col-lg-4 col-md-12 col-sm-12">
					<div class="blog-interna__form">
						<form>
							<p>Assine nossos<br/> posts.</p>
							
							<input type="text" name="email" placeholder="Seu melhor e-mail" />
							<input class="btn btn-primary" type="submit" value="Quero receber" />
						</form>
					</div>

					<div class="blog-interna__pop">
						<h2 class="title title--small">Posts mais Lidos</h2>

						<article class="blog__post--large clearfix">
							<a class="blog__thumbnail" href="blog_interna.php">
								<h3 class="blog__title blog__title">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
								</h3>
								<p class="blog__author">
									Por: Gilberto Zorzo
								</p>
								<p class="blog__text blog__text">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut...
								</p>
							</a>
							<a class="blog__link blog__link" href="blog_interna.php">Continuar Lendo >></a>
						</article>

						<article class="blog__post--large clearfix">
							<a class="blog__thumbnail" href="blog_interna.php">
								<h3 class="blog__title blog__title">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
								</h3>
								<p class="blog__author">
									Por: Gilberto Zorzo
								</p>
								<p class="blog__text blog__text">
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut...
								</p>
							</a>
							<a class="blog__link blog__link" href="blog_interna.php">Continuar Lendo >></a>
						</article>
					</div>

					<div class="blog-interna__category">
						<h2 class="title title--small">Categorias</h2>

						<ul>
							<li>
								<a href="" class="active">Lorem ipsum dolor sit amet,</a>
							</li>
							<li>
								<a href="">Lorem ipsum dolor sit amet,</a>
							</li>
							<li>
								<a href="">Lorem ipsum dolor sit amet,</a>
							</li>
							<li>
								<a href="">Lorem ipsum dolor sit amet,</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>