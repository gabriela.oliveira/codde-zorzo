<?php require_once('../components/header.php'); ?>

<main class="main-travel" role="main">

	<section class="main__title paciente-em-viagem">
		<div class="container">
			<h2 class="title">Pacientes em Viagens</h2>
			<p class="title__sub">Veja em qualquer lugar</p>
		</div>
	</section>

	<section class="travel__content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-sm-12">
					<div class="travel__listing">
						<h2 class="header">Pré-consulta</h2>

						<p>
							Em nossa prática, recebemos pacientes que moram em localidades distantes de Florianópolis, de outros Estados e países e pensando em bem atende-los, destinamos este espaço para diminuirmos esta distância inicial.
						</p>

						<p>
							Caso este seja seu perfil, esclarecemos que suas informações pessoais não serão compartilhadas com ninguém além do nosso corpo de profissionais.
						</p>

						<p>
							Caso você esteja interessado(a) em uma avaliação preliminar, ou se tem dúvidas, <span>preencha o formulário ao lado</span>
						</p>

						<h3>Aqui está o processo para pacientes de fora:</h3>

						<div class="list__number">
							<span>1-</span>
							<p>
								Entre em contato conosco, pelo nosso website, e informe sobre a cirurgia que lhe interessa e ele lhe enviará um formulário de registro para alguns detalhes.<span>*</span>
							</p>
						</div>

						<div class="list__number">
							<span>2-</span>
							<p>
								Então, se você deseja uma média sobre os valores envolvidos, ele vai pedir que você nos envie algumas fotos para análise médica.
							</p>
						</div>

						<div class="list__number">
							<span>3-</span>
							<p>
								Em acordo com os valores, ele terá que registrar e solicitar os exames pré-operatórios. Estes exames devem ser feitos em sua cidade para ter certeza que você está em boas condições de saúde para ser submetido à cirurgia proposta. O registro e exames devem ser enviados por e-mail ou fax. Os exames devem provavelmente ser repetidos no Brasil para os pacientes estrangeiros.							
							</p>
						</div>

						<div class="list__number">
							<span>4-</span>
							<p>
								Depois de analisar as informações e aprovação médica de sua cirurgia, ele será capaz de agendar a data de sua preferência para a sua chegada a Florianópolis.
							</p>
						</div>

						<div class="list__number">
							<span>5-</span>
							<p>
								Para o seu conforto, se necessário, nós poderemos indicar uma agência de viagens que ajudará com os vôos e reservas de hotel com melhores custos. Conheça <a href=""><span>AQUI</span></a> algumas sugestões de hotéis.							
							</p>
						</div>

						<div class="list__number">
							<span>6-</span>
							<p>
								Então, logo que você desejar, ele poderá agendar sua cirurgia.
							</p>
						</div>

						<div class="list__number">
							<span>7-</span>
							<p>
								Após a cirurgia, você vai ficar no hospital por oito a vinte e quatro horas, retornando em seguida para o local que está hospedado, onde vai ficar de sete a dez dias, pelo menos, dependendo da cirurgia. Durante este período, ele vai acompanhar a sua recuperação pós-operatória, com períodos de consultas, curativos e outros procedimentos, até que as condições sejam consideradas perfeitas para seu retorno à sua cidade.
							</p>
						</div>

						<div class="list__number">
							<span>*</span>
							<p>
								Este processo não substituirá a consulta médica pessoal com o Dr. Henrique Müller que acontecerá em todos os casos previamente à cirurgia. 
							</p>
						</div>

						<h2>Questionamentos Frequentes</h2>
						
						<h3>
							É seguro viajar após um procedimento cirúrgico?
						</h3>

						<p>
							Seus planos de viagem devem ser inteiramente ditados pelo procedimento cirúrgico recomendado. Depois de selecionar um procedimento, nós iremos informá-lo sobre o tempo recomendado exigido para recuperação e permanência em Florianópolis antes do retorno e ele não aconselha a realização de passeios turísticos após a cirurgia, seria melhor programar para fazê-los antes da cirurgia sempre.
						</p>

						<h3>
							Tenho medo que algo dê errado após a cirurgia. Como diminuir esta ansiedade?
						</h3>
						<p>
							Os nossos pacientes conhecem o seu intenso comprometimento em alcançar exatamente o que esperavam sobre sua cirurgia.
						</p>
						<p>
							Durante sua estada em Florianópolis, você terá acesso contínuo aos nossos profissionais para esclarecer qualquer dúvida médica e compartilhar preocupações, uma vez que você volte para casa, ele pode se comunicar com médicos locais em caso de necessidade, o que é raro.
						</p>
						<p>
							Os pacientes de outros Estados do Brasil e estrangeiros sentem-se satisfeitos e encontram ainda vários benefícios, entre quais poderia citar: a oportunidade de conhecer nossas praias, cultura e o carinho do povo catarinense, e ao retonarem para suas origens, levam lindas lembranças, além da satisfação do alcance de seu objetivo quanto sua cirurgia; dispõe do anonimato que não teriam em sua cidade, relaxam e se recuperaram com maior qualidade, desvinculados das tensões cotidianas e demandas.
						</p>

						<h3>
							Precisarei de atendimento médico depois de eu voltar para casa?
						</h3>
						<p>
							Geralmente não há necessidade de desde que seja respeitado o período recomendado de permanência no Brasil (cidade de Florianópolis) para sua recuperação pós-operatória.
						</p>
						<p>
							Depois do seu retorno, vamos manter contato por e-mail para responder a quaisquer dúvidas que você possa ter e o profissional responsavel irá orientar você em sua recuperação.
						</p>

						<h3>
							Quais expectativas devo ter sobre a consulta? 
						</h3>

						<p>
							A melhor maneira de descobrir se a Cirurgia é indicada para você, é através da consulta médica com um dos nossos profissionais. 
						</p>

						<p>
							Ele vai ouvir você, examiná-lo(a) e lhe fornecer orientações apropriadas. Fará parte deste processo , a discussão sobre o procedimento que você está interessado(a) em, ou recomendar outro específico para a sua necessidade, como também explicações a para ajudá-lo (a) a visualizar os resultados e também discorrer sobre os riscos. 
						</p>
						
					</div>
				</div>

				<div class="col-lg-4 col-md-12 col-sm-12">
					<div class="travel__form">
						<form>
							<p>Quero fazer uma pré-consulta.</p>
							
							<input type="text" name="name" placeholder="Seu nome" />
							<input type="text" name="email" placeholder="Seu e-mail" />
							<input type="number" name="email" placeholder="Seu telefone" />
							<textarea name="textarea" placeholder="Faça sua pré-consulta aqui" ></textarea>
							<input class="btn btn-primary" type="submit" value="Enviar pré-consulta" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>