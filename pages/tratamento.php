<?php require_once('../components/header.php'); ?>

<main class="main-doc" role="main">

	<section class="main__title doc">
		<div class="container">
			<h2 class="title">Procedimentos Estético</h2>
			<p class="title__sub">Conheça o que fazemos</p>
		</div>
	</section>

	<section class="doc__content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-8 col-sm-12">
					<div class="doc__listing">
						<h2>Procedimentos Estéticos</h2>

						<p>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
						</p>

						<div class="doc__video">
							<iframe width="750" height="300" src="https://www.youtube.com/embed/vt1Pwfnh5pc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						</div>
						
						<ul>
							<li>Botox</li>
							<li>Botox</li>
							<li>Preenchimento e aplicação para tirar manchas na pele,</li>
						</ul>
						
						<h3>
							Botox
						</h3>

						<p>
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
						</p>

						<h3>
							Preenchimento e aplicação para tirar manchas na pele	
						</h3>
						
						<p>
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
						</p>

						<h3>
							bichectomia.
						</h3>

						<p>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
						</p>
					</div>
				</div>

				<div class="col-lg-4 col-md-12 col-sm-12">
					<div class="doc__form">
						<form>
							<p>Quero receber dicas de saúde e estética.</p>
							
							<input type="text" name="email" placeholder="Seu melhor e-mail" />
							<input class="btn btn-primary" type="submit" value="Quero receber" />
						</form>
					</div>
					
					<div class="doc__form doc__form--consulta">
						<form>
							<p>Quero fazer uma pré-consulta.</p>
							
							<input type="text" name="name" placeholder="Seu nome" />
							<input type="text" name="email" placeholder="Seu e-mail" />
							<textarea name="textarea" placeholder="Faça sua pré-consulta aqui" ></textarea>
							<input class="btn btn-primary black" value="Anexar Foto" />
							<input class="btn btn-primary" type="submit" value="Enviar pré-consulta" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	
</main>

<?php require_once('../components/footer.php'); ?>